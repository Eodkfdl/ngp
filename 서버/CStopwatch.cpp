#include "CStopwatch.h"
#include "stdafx.h"


CStopwatch::CStopwatch() { QueryPerformanceFrequency(&m_liPerfFreq); Start(); }
CStopwatch::~CStopwatch() {};

void CStopwatch::Start() { QueryPerformanceCounter(&m_liPerfStart); }

__int64 CStopwatch::Now() const {
	LARGE_INTEGER liPerfNow;
	QueryPerformanceCounter(&liPerfNow);
	return (((liPerfNow.QuadPart - m_liPerfStart.QuadPart) * 1000)
		/ m_liPerfFreq.QuadPart);
}

__int64 CStopwatch::NowInMicro() const {
	LARGE_INTEGER liPerfNow;
	QueryPerformanceCounter(&liPerfNow);
	return (((liPerfNow.QuadPart - m_liPerfStart.QuadPart) * 100000)
		/ m_liPerfFreq.QuadPart);
}