#pragma once

class Object
{
public:
	Object();
	~Object();
	void initPhysics();
	void Update(float elapsedsec);
	void AddForce(float x, float y, float z, float elapsedTime);
	void SetColor(float r, float g, float b, float a);
	void GetColor(float* r, float* g, float* b, float* a);
	void GetPos(float *x, float *y, float *z);
	void SetPos(float x, float y, float z);
	void GetMass(float *m);
	void SetMass(float m);
	void SetVel(float x, float y, float z);
	void GetVel(float* x, float* y, float* z);
	void SetAcc(float x, float y, float z);
	void GetAcc(float* x, float* y, float* z);
	void SetVol(float x, float y, float z);
	void GetVol(float* x, float* y, float* z);
	void GetFriction(float* f);
	void SetFriction(float f);
	void Settxtureid(int t);
	void Gettxtureid(int* t);
	void CoordinateOverride(float* x, float* y, float* z, float* sx, float* sy, float* sz);//충돌체크에서는 양수만을이용하고 랜더링할땐 원래위치값으로 바꿔준다
	void SetCategory(int category);

		void GetCategory(int* category);


		void SetCollided(int collided);

	void GetCollided(int* collided);
	
private:
	int ready;
	int m_category;
	int m_collided;
	bool m_turn;
	float m_r, m_g, m_b, m_a;
	float m_posX, m_posY, m_posZ;
	float m_mass;
	float m_velX, m_velY, m_velZ;
	float m_accX, m_accY, m_accZ;
	float m_volX, m_volY, m_volZ;
	float m_friction;
	float m_remainingCooltime;

	int m_txtureid;

};