#include "stdafx.h"
#include "Object.h"
#include "Globals.h"
#include <math.h>
#include <float.h>
Object::Object()
{
	initPhysics();
}

Object::~Object()
{

}
void Object::CoordinateOverride(float* x, float* y, float* z, float*sx, float* sy, float* sz) {
	*x -= WindowX/200;
	*y -= WindowY/200;
	//*z -= 400;

	*x *=  100.f;
	*y = *y * 100.f;
	*z = *z * 100.f;

	*sx = *sx * 100.f;
	*sy = *sy * 100.f;
	*sz = *sz * 100.f;
	// 1 meter = 100 cm == 100 pixels

}

void Object::Update(float elapsedTime) {

	float nForce = m_mass * GRAVITY;//수직항력의 사이즈
	float fForce = m_friction * nForce;//마찰력의 크기
	float velSize = sqrtf(m_velX*m_velX+m_velY*m_velY);
	m_remainingCooltime -= elapsedTime;
	if (velSize > 0.f) {

		float fDirX = -1.f * m_velX / velSize;//x축에 적용하는 마찰려ㄱ방향
		float fDirY = -1.f * m_velY / velSize;
		fDirX = fDirX * fForce;
		fDirY = fDirY * fForce;
		float fAccx = fDirX / m_mass;
		float fAccy = fDirY / m_mass;
		// 속도가 0이되면 
		
			float newx = m_velX + fAccx * elapsedTime;
			float newy = m_velY + fAccy * elapsedTime;

	   
		
			if (newx * m_velX < 0.f) {
				m_velX = 0.f;
			}
			else {
				m_velX = newx;
			}
			if (newy * m_velY < 0.f) {
				m_velY = 0.f;
			}
			else
			{
				m_velY = newy;
			}
		
	}


	m_posX += m_velX * elapsedTime;
	m_posY += m_velY * elapsedTime;
	m_posZ += m_velZ * elapsedTime;
}
void Object::initPhysics(){
	m_r, m_g, m_b, m_a = 0.f;
	m_mass = 0.f;
	m_posX, m_posY, m_posZ = 0.f;
	m_velX, m_velY, m_velZ = 0.f;
	m_accX, m_accY, m_accZ = 0.f;
	m_volX, m_volY, m_volZ = 0.f;
	m_friction = 0.f;

};
void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}
void Object::AddForce(float x, float y, float z, float elapsedTime) {
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX += accX * elapsedTime;
	m_velY += accY * elapsedTime;
	m_velZ += accZ * elapsedTime;
}
void Object::GetPos(float *x, float *y, float *z) {

	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}
void Object::SetPos(float x, float y, float z) {

	m_posX = x;
	m_posY = y;
	m_posZ = z;
}
void  Object::GetMass(float *m) {
	*m = m_mass;
}
void  Object::SetMass(float m) {
	m_mass = m;
}
void Object::SetVel(float x, float y, float z) {
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}
void  Object::GetVel(float* x, float* y, float* z) {
	*x=m_velX ;
	*y=m_velY ;
	*z=m_velZ ;
}
void  Object::SetAcc(float x, float y, float z) {
	m_accX = x;
	m_accY = y;
	m_accZ = z;

}
void  Object::GetAcc(float* x, float* y, float* z) {
	*x= m_accX ;
	*y=m_accY ;
	*z= m_accZ ;
}
void Object::SetVol(float x, float y, float z) {
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}
void  Object::GetVol(float* x, float* y, float* z) {
	*x= m_volX ;
	*y=m_volY ;
	*z= m_volZ;
}
void Object::GetFriction(float* f) {
	*f = m_friction;
}

void Object::SetFriction(float f) {
	m_friction = f;
}

void Object::Settxtureid(int t) {
	m_txtureid = t;
}
void Object::Gettxtureid(int* t) {
	*t = m_txtureid;
}

void Object::SetCategory(int category)
{
	m_category = category;
}
void Object::GetCategory(int* category)
{
	*category = m_category;
}

void Object::SetCollided(int collided)
{
	m_collided = collided;
}
void Object::GetCollided(int* collided)
{
	*collided = m_collided;
}
