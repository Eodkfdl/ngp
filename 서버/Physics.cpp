#include "stdafx.h"
#include "Physics.h"

Physics::Physics() {}
Physics::~Physics(){}
bool Physics::IsOverlap(Object * a , Object* b,int method){
	switch (method)
	{
	case 0://bb오버랩테스트
		return BBoverlap(a, b);
		break;

	default:
		break;
	}
	return false;

}
bool Physics::BBoverlap(Object* a, Object* b) {
	float ax, ay, az;//a오브젝트에서필요한거ㅓㅅ
	float asx, asy, asz;
	float aMinx, aMiny, aMinz;
	float aMaxx, aMaxy, aMaxz;
	a->GetPos(&ax, &ay, &az);
	a->GetVol(&asx, &asy, &asz);
	aMinx = ax - asx / 2.f;
	aMiny = ay - asy / 2.f;
	aMinz = az - asz / 2.f;
	aMaxx = ax + asx / 2.f;
	aMaxy = ay + asy / 2.f;
	aMaxz = az + asz / 2.f;

	float bx, by, bz;
	float bsx, bsy, bsz;
	float bMinx, bMiny, bMinz;
	float bMaxx, bMaxy, bMaxz;

	b->GetPos(&bx, &by, &bz);
	b->GetVol (&bsx, &bsy, &bsz);
	bMinx = bx - bsx / 2.f;
	bMiny = by - bsy / 2.f;
	bMinz = bz - bsz / 2.f;
	bMaxx = bx + bsx / 2.f;
	bMaxy = by + bsy / 2.f;
	bMaxz = bz + bsz / 2.f;

	if (aMinx > bMaxx)
		return false;
	if (aMaxx < bMinx)
		return false;
	if (aMiny > bMaxy)
		return false;
	if (aMaxy < bMiny)
		return false;

	if (aMinz > bMaxz)
		return false;
	if (aMaxz < bMinz)
		return false;
	
	return true;

}

void Physics::Processcollsion(Object* a, Object* b){

	float amass, avx, avy, avz;
	float afvx, afvy, afvz;//충돌후 속도
	a->GetMass(&amass);
	a->GetVel(&avx, &avy, &avz);
	float bmass, bvx, bvy, bvz;
	float bfvx, bfvy, bfvz;//충돌후 속도
	b->GetMass(&bmass);
	b->GetVel(&bvx, &bvy, &bvz);

	afvx = ((amass - bmass) / (amass + bmass)) * avx+ ((bmass*2.f)/(amass+bmass))*bvx;
	afvy = ((amass - bmass) / (amass + bmass)) * avy + ((bmass * 2.f) / (amass + bmass)) * bvy;
	afvz = ((amass - bmass) / (amass + bmass)) * avz + ((bmass * 2.f) / (amass + bmass)) * bvz;

	bfvx = ((amass * 2.f) / (amass + bmass)) * avx + ((bmass - amass) / (amass + bmass)) * bvx;
	bfvy = ((amass * 2.f) / (amass + bmass)) * avy + ((bmass - amass) / (amass + bmass)) * bvy;
	bfvz = ((amass * 2.f) / (amass + bmass)) * avz + ((bmass - amass) / (amass + bmass)) * bvz;
	

	a->SetVel(afvx, afvy, afvz);

	b->SetVel(bfvx, bfvy, bfvz);
}