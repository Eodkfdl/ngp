#include "stdafx.h"
#include <Windows.h>
#include <Winbase.h>

class CStopwatch
{
public:
	CStopwatch();
	~CStopwatch();

	void Start();

	__int64 Now() const;

	__int64 NowInMicro() const;

private:
	LARGE_INTEGER m_liPerfFreq;
	LARGE_INTEGER m_liPerfStart;
};

