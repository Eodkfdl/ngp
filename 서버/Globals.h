#pragma once

#define _WINSOCK_DEPRECATED_NO_WARNINGS 
#define SERVERPORT 9000
#define BUFSIZE   1024*1
#define HERO_ID 0

#define MAX_OBJECTS 25

#define GRAVITY 9.8f
//윈도우사이즈 조절
#define WindowX 1280
#define WindowY 1024

#define TYPE_NOMAL 0
#define TYPE_BULLET 1

//num이붙은 애들은 오브젝트 넘버이다.

#define Num_BALL 0
#define Num_WALL 1
#define Num_WALLR 2
#define Num_WALLL 3
#define Num_PLAYER1 4
#define Num_PLAYER2 5

#define Num_Start 9
#define Num_EndW   10
#define Num_EndL  11
#define Num_Ready 12
#define Num_sready 13
#define Num_swait 14
#define Num_player1 15
#define Num_player2 16

#define Scn_Ready 1
#define Scn_Start 2
#define Scn_Game 3
#define Scn_EndW 4
#define Scn_EndL 5



