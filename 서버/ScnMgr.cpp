#include "stdafx.h"
#include "ScnMgr.h"
#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#pragma warning(disable:4996)
#pragma comment(lib, "ws2_32")
#include <winsock2.h>
#include <stdlib.h>
#include <stdio.h>
#define SERVERIP   "127.0.0.1"
#define SERVERPORT 9000
#define BUFSIZE    512

ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(WindowX, WindowY);
	m_Physics = new Physics();//해당클래스를 빼내어서 네트워크쪽에 넣으면됨

	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}
	
	int texid[10];
	texid[0]= m_Renderer->GenPngTexture("./texture/ball.png");

	texid[1] = m_Renderer->GenPngTexture("./texture/ready.png");

	texid[2] = m_Renderer->GenPngTexture("./texture/sready.png");

	texid[3] = m_Renderer->GenPngTexture("./texture/swait.png");
	texid[4] = m_Renderer->GenPngTexture("./texture/player1.png");
	texid[5] = m_Renderer->GenPngTexture("./texture/player2.png");
	
	texid[6] = m_Renderer->GenPngTexture("./texture/Start.png");

	texid[7] = m_Renderer->GenPngTexture("./texture/win.png");

	texid[8] = m_Renderer->GenPngTexture("./texture/lose.png");
	// Init Test Object
	
	// Init Objects
	for (int i = 0; i < MAX_OBJECTS; ++i) {
		m_Object[i] = NULL;
	}


	// 볼생성 
	m_Object[Num_BALL] = new Object();
	m_Object[Num_BALL]->SetColor(1, 0, 0, 0);
	m_Object[Num_BALL]->SetPos(0, 0, 0);
	m_Object[Num_BALL]->SetVol(0.25, 0.25, 0.25);
	m_Object[Num_BALL]->SetVel(0, 0, 0);
	m_Object[Num_BALL]->SetMass(1);
	m_Object[Num_BALL]->SetFriction(1.0f);
	m_Object[Num_BALL]->Settxtureid(texid[0]);

	//벽생성
	//벽은 1~3사이의값을가짐

	m_Object[Num_WALL] = new Object();
	m_Object[Num_WALL]->SetColor(1, 0, 0, 1);
	m_Object[Num_WALL]->SetPos(6, 5, 0);
	m_Object[Num_WALL]->SetVol(12.8, 0.125, 1);
	m_Object[Num_WALL]->SetVel(0, 0, 0);
	m_Object[Num_WALL]->SetMass(1000);
	m_Object[Num_WALL]->SetFriction(1.0f);

	m_Object[Num_WALLR] = new Object();
	m_Object[Num_WALLR]->SetColor(1, 0, 0, 1);
	m_Object[Num_WALLR]->SetPos(14, 5, 0);
	m_Object[Num_WALLR]->SetVol(4, 11, 1);
	m_Object[Num_WALLR]->SetVel(0, 0, 0);
	m_Object[Num_WALLR]->SetMass(1000);
	m_Object[Num_WALLR]->SetFriction(1.0f);

	m_Object[Num_WALLL] = new Object();
	m_Object[Num_WALLL]->SetColor(1, 0, 0, 1);
	m_Object[Num_WALLL]->SetPos(-2, 5, 0);
	m_Object[Num_WALLL]->SetVol(4, 11, 1);
	m_Object[Num_WALLL]->SetVel(0, 0, 0);
	m_Object[Num_WALLL]->SetMass(1000);
	m_Object[Num_WALLL]->SetFriction(1.0f);
	// 플레이어1 생성
	// 플레이어는 
	m_Object[Num_PLAYER1] = new Object();
	m_Object[Num_PLAYER1]->SetColor(1, 0, 0, 1);
	m_Object[Num_PLAYER1]->SetPos(6, 0.0, 0);
	m_Object[Num_PLAYER1]->SetVol(1, 0.2, 1);
	m_Object[Num_PLAYER1]->SetVel(0, 0, 0);
	m_Object[Num_PLAYER1]->SetMass(1);
	m_Object[Num_PLAYER1]->SetFriction(1.0f);


	m_Object[Num_PLAYER2] = new Object();
	m_Object[Num_PLAYER2]->SetColor(1, 0, 0, 1);
	m_Object[Num_PLAYER2]->SetPos(6, 9.85, 0);
	m_Object[Num_PLAYER2]->SetVol(1, 0.2, 1);
	m_Object[Num_PLAYER2]->SetVel(0, 0, 0);
	m_Object[Num_PLAYER2]->SetMass(1);
	m_Object[Num_PLAYER2]->SetFriction(1.0f);
	//대기화면

	m_Object[Num_Ready] = new Object();
	m_Object[Num_Ready]->SetColor(1, 0, 0, 0);
	m_Object[Num_Ready]->SetPos(6.0, 5.0, 0);
	m_Object[Num_Ready]->SetVol(12.8, 10.28, 0.25);
	m_Object[Num_Ready]->SetVel(0, 0, 0);
	m_Object[Num_Ready]->SetMass(1);
	m_Object[Num_Ready]->Settxtureid(texid[1]);


	m_Object[Num_sready] = new Object();
	m_Object[Num_sready]->SetColor(1, 0, 0, 0);
	m_Object[Num_sready]->SetPos(6.0, 7.0, 0);
	m_Object[Num_sready]->SetVol(1.5, 1.5, 0.25);
	m_Object[Num_sready]->SetVel(0, 0, 0);
	m_Object[Num_sready]->SetMass(1);
	m_Object[Num_sready]->Settxtureid(texid[2]);

	m_Object[Num_swait] = new Object();
	m_Object[Num_swait]->SetColor(1, 0, 0, 0);
	m_Object[Num_swait]->SetPos(6.0, 9.0, 0);
	m_Object[Num_swait]->SetVol(1.5, 1.5, 0.25);
	m_Object[Num_swait]->SetVel(0, 0, 0);
	m_Object[Num_swait]->SetMass(1);
	m_Object[Num_swait]->Settxtureid(texid[3]);

	m_Object[Num_player1 ] = new Object();
	m_Object[Num_player1]->SetColor(1, 0, 0, 0);
	m_Object[Num_player1]->SetPos(2.0, 7.0, 0);
	m_Object[Num_player1]->SetVol(1.5, 1.5, 0.25);
	m_Object[Num_player1]->SetVel(0, 0, 0);
	m_Object[Num_player1]->SetMass(1);
	m_Object[Num_player1]->Settxtureid(texid[4]);

	m_Object[Num_player2] = new Object();
	m_Object[Num_player2]->SetColor(1, 0, 0, 0);
	m_Object[Num_player2]->SetPos(2.0, 9.0, 0);
	m_Object[Num_player2]->SetVol(1.5, 1.5, 0.25);
	m_Object[Num_player2]->SetVel(0, 0, 0);
	m_Object[Num_player2]->SetMass(1);
	m_Object[Num_player2]->Settxtureid(texid[5]);

	//시작화면 
	m_Object[Num_Start] = new Object();
	m_Object[Num_Start]->SetColor(1, 0, 0, 0);
	m_Object[Num_Start]->SetPos(6.0, 5.0, 0);
	m_Object[Num_Start]->SetVol(12.8, 10.28, 0.25);
	m_Object[Num_Start]->SetVel(0, 0, 0);
	m_Object[Num_Start]->SetMass(1);
	m_Object[Num_Start]->Settxtureid(texid[6]);
	//종료화면
	m_Object[Num_EndW] = new Object();
	m_Object[Num_EndW]->SetColor(1, 0, 0, 0);
	m_Object[Num_EndW]->SetPos(6.0, 5.0, 0);
	m_Object[Num_EndW]->SetVol(12.8, 10.28, 0.25);
	m_Object[Num_EndW]->SetVel(0, 0, 0);
	m_Object[Num_EndW]->SetMass(1);
	m_Object[Num_EndW]->Settxtureid(texid[7]);

	m_Object[Num_EndL] = new Object();
	m_Object[Num_EndL]->SetColor(1, 0, 0, 0);
	m_Object[Num_EndL]->SetPos(6.0, 5.0, 0);
	m_Object[Num_EndL]->SetVol(12.8, 10.28, 0.25);
	m_Object[Num_EndL]->SetVel(0, 0, 0);
	m_Object[Num_EndL]->SetMass(1);
	m_Object[Num_EndL]->Settxtureid(texid[8]);
	
}
int ScnMgr::GetScnNum() {
	return Scn_Num;
}
ScnMgr::~ScnMgr()
{
	delete m_Renderer;
	m_Renderer = NULL;
}



void ScnMgr::Update(float elapsedTimeInSec) {
	
	char buf[BUFSIZE + 1];
	int len;
	int sendflag = 0;
	char *massage[] = {
		"A",
		"D"
	};
	float fx = 0.f, fy = 0.f, fz = 0.f;
	float fAmount = 25;

	if (m_KeyA == true) {
		fx -= 1.0f;
		sendflag = 1;
		len = strlen(massage[0]);
		strncpy(buf, massage[0], len);
	}
	if (m_KeyD == true) {
		fx += 1.0f;
		sendflag = 1;
		len = strlen(massage[1]);
		strncpy(buf, massage[1], len);
	}

	if (sendflag == 1)
	{
		//retval = send(sock, buf, len, 0);
		/*if (retval == SOCKET_ERROR)
		{
			err_display("send()");
		}*/
	}

	float fSize = sqrtf(fx * fx + fy * fy);
	if (fSize > FLT_EPSILON) {
		fx /= fSize;
		fx *= fAmount;
		fy /= fSize;
		fy *= fAmount;
	}
	// Add Control Force To Hero
	m_Object[Num_PLAYER1]->AddForce(fx, fy, fz, elapsedTimeInSec);
	
	float hx=0, hy=0, hz=0;

	m_Object[Num_PLAYER1]->GetPos(&hx, &hy, &hz);
	m_Object[Num_BALL]->SetPos(hx, hy+0.25, hz);//레디상태이면 공을 플레이어 위에다 붙여놓기
	for (int src = 0; src < Num_PLAYER2; src++) {//탄성충돌체크부분
		for (int dst = src + 1; dst < Num_PLAYER2+1; dst++) {
			if (m_Object[src] != NULL && m_Object[dst] != NULL)
				if (m_Physics->IsOverlap(m_Object[src], m_Object[dst])) {
					printf("Collision : %d, %d\n", src, dst);

						m_Physics->Processcollsion(m_Object[src], m_Object[dst]);
					
				}
		}
	}
	for (int i = 0; i < MAX_OBJECTS; i++) {
		if (m_Object[i] != NULL)
			m_Object[i]->Update(elapsedTimeInSec);
	}
}

void ScnMgr::RenderScene(void) {//씬넘버에따라 화면에 표시되는 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.5f, 0.5f, 1.0f, 1.0f);
	float x, y, z = 0;
	float sx, sy, sz = 0;
	float r, g, b, a = 0;
	int texid = -1;
	if (GetScnNum() == Scn_Ready){

		for (int i = 0; i < 5; i++) {
			m_Object[Num_Ready + i]->GetPos(&x, &y, &z);
			m_Object[Num_Ready + i]->GetVol(&sx, &sy, &sz);
			m_Object[Num_Ready + i]->GetColor(&r, &g, &b, &a);
			m_Object[Num_Ready+i]->Gettxtureid(&texid);
			m_Object[Num_Ready+i]->CoordinateOverride(&x, &y, &z, &sx, &sy, &sz);

			m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texid);
		
		}
	} // 레디상태일때 띄워주는 화면
	if (GetScnNum() == Scn_Start){//게임시작시 잠깐보여주고 넘어가는화면
	

		m_Object[Num_Start]->GetPos(&x, &y, &z);
		m_Object[Num_Start]->GetVol(&sx, &sy, &sz);
		m_Object[Num_Start]->GetColor(&r, &g, &b, &a);
		m_Object[Num_Start]->Gettxtureid(&texid);
		m_Object[Num_Start]->CoordinateOverride(&x, &y, &z, &sx, &sy, &sz);

		m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texid);
	}
	if (GetScnNum() == Scn_Game){//실제게임중일때 렌더링

		for (int i = 0; i < 7; i++) {
		
			if (m_Object[i] != NULL) {
				m_Object[i]->GetPos(&x, &y, &z);
				m_Object[i]->GetVol(&sx, &sy, &sz);
				m_Object[i]->GetColor(&r, &g, &b, &a);
				m_Object[i]->Gettxtureid(&texid);
				m_Object[i]->CoordinateOverride(&x, &y, &z, &sx, &sy, &sz);

				m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texid);
				if (m_Object[Num_PLAYER1]) {
					cout << x << "," << y << endl;
				}
			}
		}
	}
	if (GetScnNum() == Scn_EndW) {

		m_Object[Num_EndW]->GetPos(&x, &y, &z);
		m_Object[Num_EndW]->GetVol(&sx, &sy, &sz);
		m_Object[Num_EndW]->GetColor(&r, &g, &b, &a);
		m_Object[Num_EndW]->Gettxtureid(&texid);
		m_Object[Num_EndW]->CoordinateOverride(&x, &y, &z, &sx, &sy, &sz);

		m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texid);
	}// 게임종료시 보여주는화면 승리 패배 오브젝트 추가필요
	if (GetScnNum()== Scn_EndL){


		m_Object[Num_EndL]->GetPos(&x, &y, &z);
		m_Object[Num_EndL]->GetVol(&sx, &sy, &sz);
		m_Object[Num_EndL]->GetColor(&r, &g, &b, &a);
		m_Object[Num_EndL]->Gettxtureid(&texid);
		m_Object[Num_EndL]->CoordinateOverride(&x, &y, &z, &sx, &sy, &sz);

		m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texid);
	}
}

int ScnMgr::AddObject(float x, float y, float z,
float sx, float sy, float sz,
float r, float g, float b, float a,
float vx, float vy, float vz,
float mass,
float fric,
int type)
{
	// Search empty slot
	int idx = -1;
	for (int i = 0; i < MAX_OBJECTS; i++) {
		if (m_Object[i] == NULL) {
			idx = i;
			break;
		}
	}
	if (idx == -1) {
		std::cout << "No more remaining object \n";
		return -1;
	}

	m_Object[idx] = new Object();
	m_Object[idx]->SetColor(r, g, b, a);
	m_Object[idx]->SetPos(x, y, z);
	m_Object[idx]->SetVol(sx, sy, sz);
	m_Object[idx]->SetVel(vx, vy, vz);
	m_Object[idx]->SetMass(mass);
	m_Object[idx]->SetFriction(fric);


	return idx;
}

void ScnMgr::DeleteObject(int idx)
{
	// idx 의 범위 오류 검사 추가해야됨
	if (idx < 0) {
		std::cout << "Negative idx does not allowed. \n";
		return;
	}
	if (idx >= MAX_OBJECTS) {
		std::cout << "Requested idx exceeds MAX_OBJECTS count. \n";
		return;
	}
	if (m_Object[idx] != NULL) {
		delete m_Object[idx];
		m_Object[idx] = NULL;
	}
}


void ScnMgr::KeyDownInput(unsigned char key, int x, int y) {
	if (key == 'a' || key == 'A') {
		m_KeyA = true;
	}
	if (key == 'd' || key == 'D') {
		m_KeyD = true;
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y) {
	if (key == 'a' || key == 'A') {
		m_KeyA = false;
	}
	if (key == 'd' || key == 'D') {
		m_KeyD = false;
	}
	if (key == 'r' || key == 'R') {//레디키를 입력 -> 플레이어1의 상태를
		Scn_Num = 3;
	}
	if (key == '4' || key == '$') {
		Scn_Num = 4;
	}
	if (key == '5' || key == '%') {
		Scn_Num = 5;
	}
	if (key == '3' || key == '#') {
		Scn_Num = 3;
	}
	if (key == '2' || key == '@') {
		Scn_Num = 2;
	}
	if (key == '1' || key == '!') {
		Scn_Num = 1;
	}
	if (key == 'g' || key == 'G') {//게임시작

	}
	
}