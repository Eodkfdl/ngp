#pragma once
#include "Renderer.h"
#include "Object.h"
#include "Globals.h"
#include "Physics.h"
#include "Dependencies\freeglut.h"
class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();


	void Update(float elapsedTimeInSec);
	void RenderScene(void);
	int AddObject(float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a,
		float vx, float vy, float v,
		float mass, 
		float fric,
		int type);
	void DeleteObject(int idx);

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);

	int GetScnNum();
private:
	Renderer* m_Renderer = NULL;
	Physics* m_Physics = NULL;
	Object* m_TestObject = NULL;
	Object* m_Object[MAX_OBJECTS];
	int m_TestIdx = -1;
	int m_TestIdxArray[MAX_OBJECTS];
	int Scn_Num=1;
	bool m_KeyA = false;
	bool m_KeyD = false;
};

